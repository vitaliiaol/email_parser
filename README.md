# Description
**emailparser** is a command line tool developed to parse and extract data from an email message. Specifically, it detects and displays domain names and IPv4 addresses present in an email message, along with their respective counts.  
Optionally, contents of an email message headers can also be extracted if an appropriate flag is provided.

# Requirements
**emailparser** requires Python 3.x.x to be installed on your machine.

# Installation
In order to run the script, simply clone the repository contents with `git clone` command and navigate to the *email_parser* directory of the project.

# Usage

## Extract domain names and IP addresses
To parse an email message and extract domain names and IP addresses from an email message, run the following command:  
`python parse.py -e [path-to-an-email-file]`  
Example output:  
```
### DOMAINS ###  

Domain		Count  
google.com	3  
gitlab.com	1  

### IP ADDRESSES ###  

IP		Count  
0.0.0.0       	1  
127.0.0.1	3  
```  
Note that the domain name won't be extracted if the URL is malformed or incomplete.

## Extract header contents
Contents of an email message headers can be extracted in two different ways: using a straightforward string (in that case, the string has to match a desired header name exactly) or a regular expression pattern (in that case, every header of an email message is matched against a provided pattern).  
To extract header contents using string, run the following command:  
`python parse.py -e [path-to-an-email-file] --header-string [header-name]`  
On the other hand, to extract header contents using pattern, run the following command:  
`python parse.py -e [path-to-an-email-file] --header-pattern [regular-expression-pattern]`  
Note that these two commands can't be used simultaneously.  
Example output:  
```
### HEADERS ###  
Content-Type: text/plain; charset=us-ascii  
  
### DOMAINS ###  
  
Domain		Count  
google.com	3  
gitlab.com	1  
  
### IP ADDRESSES ###  
  
IP		Count  
0.0.0.0       	1  
127.0.0.1	3  
```   

## Adjust the logging level
The script makes use of logging, and the user can adjust the logging level using a command line argument. In order to adjust the logging level, run the following command:  
`python parse.py -e [path-to-an-email-file] -l [logging-level]`  
`[logging-level]` has to be equal to one of the following values: *debug*, *info*, *warning*, *error*, *critical*. Default value is *debug*.  

## Display command line arguments help
To display help regarding script's command line arguments, run:  
`python parse.py --help`
