import argparse
import email
import logging
import logging.config

from header_parser import HeaderParser
from message_parser import MessageParser


LOGGING_LEVELS = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'critical': logging.CRITICAL
}


def format_tabular(content, offset, first_field, second_field):
    """
    Display data extracted from email message in a tabular format.

    Parameters:
        content(collections.Counter): extracted data
        offset(int): length of the field
        first_field(str): name of the first field
        second_field(str): name of the second field

    Returns:
        str: tabular output in a string format.
    """

    table_header = "{first_field:{offset}}\t{second_field}\n".format(offset=offset,
                                                          first_field=first_field,
                                                          second_field=second_field)
    table_contents = ''
    for first_value, second_value in content.items():
        table_contents += "{first_value:{offset}}\t{second_value}\n".format(offset=offset,
                                                              first_value=first_value,
                                                              second_value=second_value)
    table = table_header + table_contents
    return table


def format_output(header_contents, domains, ip_addresses):
    """
    Formats output before displaying it in the terminal.

    Parameters:
        header_contents(dict): headers extracted from an email message.
        domains(collections.Counter): domain names that have been extracted from an
                                      email message and their respective counts.
        ip_addresses(collections.Counter): IPv4 addresses that have been extracted from
                                           an email message and their respective counts.

    Returns:
        str: formatted program output in a string format.
    """
    output = ''
    if header_contents:
        output += "### HEADERS ###\n"
        for header, content in header_contents.items():
            output += "{header}: {content}\n".format(
                                            header=header,
                                            content=content
            )
    output += "\n### DOMAINS ### \n"
    if domains:
        output += format_tabular(content=domains,
                       offset=len(max(domains.keys(), key=len)),
                       first_field="Domain",
                       second_field="Count")
    else:
        output += "No domain names have been found in an email.\n"
    output += "\n### IP ADDRESSES ###\n"
    if ip_addresses:
        output += format_tabular(content=ip_addresses,
                       offset=15,
                       first_field="IP",
                       second_field="Count")
    else:
        output += "No IP addresses have been found in an email.\n"
    return output


def main():
    """
    Runs the script. Contains argument parser and logging configuration and
    executes function calls that perform the email message parsing.
    """
    parser = argparse.ArgumentParser(description="""Command line tool to parse email message
                                                    and extract data from it. Specifically, a
                                                    list of IP addresses and domain names that
                                                    are present in the message along with their
                                                    respective counts. Information about specific
                                                    headers and their contents can be extracted as
                                                    well if the proper argument is provided.""")
    required_group = parser.add_argument_group('required named arguments')
    header_group = parser.add_mutually_exclusive_group()
    header_group.add_argument(
                    '--header-pattern',
                    help="A regular expression pattern to match against email message's headers."
    )
    header_group.add_argument(
                    '--header-string',
                    help="A string that has to be an exact match to the email message header name."
    )
    required_group.add_argument(
                    '-e',
                    help="Specify a path to an email file that you want to parse.",
                    required=True
    )
    parser.add_argument(
                    '-l', '--log',
                    default='debug',
                    choices=LOGGING_LEVELS.keys(),
                    help="""Use this argument to adjust logging level.
                            Allowed options are: debug, info, warning, error, critial.
                            Default value: debug."""
    )

    args, unknown = parser.parse_known_args()

    level = LOGGING_LEVELS.get(args.log)

    logging.config.fileConfig(fname='../logging.config')
    logger = logging.getLogger(__name__)
    logger.level = level
    logger.propagate = False
    logger.handlers[0].level = level


    logger.info("Retrieving email message from a path: %s", args.e)
    try:
        with open(args.e, 'r') as email_file:
            email_message = email.message_from_file(email_file)
    except FileNotFoundError as error:
        logger.error(error)
        return

    header_contents = None

    if any((args.header_string, args.header_pattern)):
        logging.info("Header parsing argument provided")
        logging.info(
                     "Header string: %s, header_pattern: %s",
                     args.header_string,
                     args.header_pattern
        )
        header_parser = HeaderParser(args.header_string, args.header_pattern)
        header_contents = header_parser.parse_headers(email_message)

    domain_parser = MessageParser(email_message)
    ip_addresses = domain_parser.parse_ips()
    domains = domain_parser.parse_domains()

    print(format_output(
            header_contents=header_contents,
            domains=domains,
            ip_addresses=ip_addresses
    ))

if __name__ == "__main__":
    main()
