import hashlib
from dataclasses import dataclass, field, InitVar
from datetime import datetime


@dataclass
class ParseResultModel:
    """
    A class designed to represent parse result data model.

    Attributes:
        email_message(str): a pseudofield that is used to calculate email_id value.
        domains(str): a field that stores domain names extracted from an email message,
                      as well as their respective counts.
        ip_addresses(str): a field that stores IPv4 addresses extracted from an email
                           message, as well as their respective counts.
        parse_time(datetime): a field that stores the date and time when parsing was
                              performed.
    """

    email_message: InitVar[str]

    domains: str
    ip_addresses: str

    parse_time: datetime = field(default_factory=datetime.utcnow())
    email_id: None

    def __post_init__(self, email_message):
        """
        Initializes post-init field email_id by calculating an email message md5 hash.

        Parameters:
            email_message(str): email message in a string format.
        """

        self.email_id = hashlib.md5(email_message.encode('utf-8')).hexdigest()
