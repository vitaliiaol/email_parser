import logging
import re


class HeaderParser:
    """
    A class for parsing an email message headers and extracting their content.

    Attributes:
        header_string(str): a string used for extracting contents from header that
                            matches it exactly.
        header_pattern(str): a pattern used for extracting contents from headers that
                             match it.
    """

    def __init__(self, header_string, header_pattern):
        """
        Initializes header parser object.

        Parameters:
            header_string(str): a string used for extracting contents from header that
                                matches it exactly.
            header_pattern(str): a pattern used for extracting contents from header that
                                 match it.
        """

        self.header_string = header_string
        self.header_pattern = header_pattern

    def parse_headers(self, message):
        """
        Parses email message headers depending on which of attributes has been provided.

        Parameters:
            message(email.Message): an email message to be parsed.

        Returns:
           dict: a dictionary that contains header name(s) and contents.
        """

        if self.header_string:
            logging.info("Parsing headers using string %s", self.header_string)
            contents = self._parse_with_string(message)
        else:
            logging.info("Parsing headers using pattern %s", self.header_pattern)
            contents = self._parse_with_pattern(message)
        return contents

    def _parse_with_string(self, message):
        """
        Parses email message headers and matches one of them using the string that has
        to match a header name exactly.

        Parameters:
            message(email.Message): an email message to be parsed.

        Returns:
            dict: a dictionary containg matched header name and its content.
        """

        return {self.header_string: message.get(self.header_string)}

    def _parse_with_pattern(self, message):
        """
        Parses email message headers and matches them using the provided pattern.

        Parameters:
            message(email.Message): an email message to be parsed.

        Returns:
            dict: a dictionary containing matched header names and their contents.
        """

        expression = re.compile(self.header_pattern)
        matches = []
        for header in message:
            match = re.match(expression, header)
            if match:
                logging.debug(
                    "%s header name is a match to %s pattern",
                    match.string,
                    self.header_pattern
                )
                matches.append(match.string)
        return {match: message[match] for match in matches}
