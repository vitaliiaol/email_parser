"""
A module that contains functions for database manipulation.
"""


TABLE_SCHEMA = """CREATE TABLE parse_results (
email_id VARCHAR(32) PRIMARY KEY, 
parse_time TIMESTAMP NOT NULL, 
domains JSON, 
ip_addresses JSON
)"""

def create_table(cursor, schema):
    """
    Creates a table for storing parse results in a database.
    Schema consists of the following fields:
        1) email_id is an email message hash to uniquely identify record in a table.
        2) parse_time is a timestamp that stores date and time when parsing was performed.
        3) domains is a JSON field that stores extracted domain names and their respective counts.
        4) ip_addresses is a JSON field that stores extracted IPv4 addresses and their respective counts.

    Parameters:
        cursor(MYSQLCursor): a database cursor object to perform table creation.
        schema(str): a table schema in a form of an SQL statement.
    """

    cursor.execute(schema)
