import ipaddress
import logging
import re
from collections import Counter


class MessageParser:

    """
    A class that parses an email message and extracts necessary data from it.

    Attributes
        message(email.Message): a message object to be parsed and to extract data from.
    """

    IP_PATTERN = re.compile(r'\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b')
    DOMAIN_PATTERN = re.compile(
                           r'(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-]{,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}'
    )
    URL_PATTERN = re.compile(r'https?://[^\s<>"]+|www\.[^\s<>"]+')

    def __init__(self, message):
        """
        Initializes message parser object.

        Parameters:
            message(email.Message): a message object to be parsed and to extract data from.
        """
        self.message = message

    def parse_ips(self):
        """
        Parses email message and extracts all valid IPv4 addresses from it.

        Returns:
            Counter: a collections.Counter object containing extracted IPv4 addresses and
                     their respective count.
        """

        logging.info("Extracting IPv4 addresses from an email message")
        ip_addresses = re.findall(self.IP_PATTERN, self.message.as_string())
        valid_ip_addresses = []
        for ip_address in ip_addresses:
            try:
                ipaddress.ip_address(ip_address)
            except ValueError as error:
                logging.warning(error)
            else:
                logging.debug("%s is a valid IPv4 address", ip_address)
                valid_ip_addresses.append(ip_address)
        return Counter(valid_ip_addresses)

    def parse_domains(self):
        """
        Parses email message and extracts all domain names from it. It doesn't account
        for malformed/incomplete URL addresses.

        Returns:
            Counter: a collections.Counter object containing extracted domain names and
                     their respective count.
        """

        logging.info("Extracting domain names from an email message")
        urls = re.findall(self.URL_PATTERN, self.message.as_string())
        domains = []
        for url in urls:
            logging.debug("Extracting domain from URL %s", url)
            domains.append(*re.findall(self.DOMAIN_PATTERN, url.replace('www', '')))
        return Counter(domains)
